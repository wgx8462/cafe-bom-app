import React from 'react';
import {Link} from "react-router-dom";

const DefaultLayout = ({ children }) => {
    return (
        <>
            <div className="Layout-logo">
                <img src={process.env.PUBLIC_URL + '/images/plogo.png'}/>
                <Link to="/" className="Layout-title">메인화면</Link>
                <Link to="/2" className="Layout-title">서브화면</Link>
            </div>
            <main>{children}</main>
            <footer></footer>
        </>
    )
}
export default DefaultLayout;