import './App.css';
import {Route, Routes} from "react-router-dom";
import DefaultLayout from "./layouts/DefaultLayout";
import TestPage1 from "./pages/TestPage1";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<DefaultLayout><TestPage1 /></DefaultLayout>} />
      </Routes>
    </div>
  );
}

export default App;
