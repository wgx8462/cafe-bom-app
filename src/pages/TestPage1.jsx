import React, {useCallback, useState} from "react"
import MenuCard from "../components/MenuCard"
import CartItem from "../components/CartItem"

const TestPage1 = () => {
    const [cart, setCart] = useState([])

    const menus = [
        {name: "아메리카노", imgUrl: "ame.gif", price: 2000},
        {name: "카페라떼", imgUrl: "latte.jpg", price: 2000},
        {name: "카푸치노", imgUrl: "cappu.gif", price: 2000},
        {name: "모카라떼", imgUrl: "mocha.gif", price: 2000},
        {name: "2아메리카노", imgUrl: "ame.gif", price: 3000},
        {name: "3카페라떼", imgUrl: "latte.jpg", price: 3000},
        {name: "4카푸치노", imgUrl: "cappu.gif", price: 3000},
        {name: "5모카라떼", imgUrl: "mocha.gif", price: 3000}
    ]

    const addToCart = useCallback(menu => {
        setCart((prevCart) => {
            const existingItem = prevCart.find((item) => item.name === menu.name)
            if (existingItem) {
                return prevCart.map((item) =>
                    item.name === menu.name
                        ? { ...item, quantity: item.quantity + 1 }
                        : item
                )
            } else {
                return [...prevCart, { ...menu, quantity: 1 }]
            }
        })
    }, [])

    const removeFromCart = useCallback(menu => {
        setCart((prevCart) => {
            return prevCart.reduce((acc, item) => {
                if (item.name === menu.name) {
                    if (item.quantity > 1) {
                        return [...acc, { ...item, quantity: item.quantity - 1 }];
                    }
                } else {
                    return [...acc, item];
                }
                return acc;
            }, [])
        })
    }, [])

    const calculateTotal = useCallback(() => {
        return cart.reduce((total, item) => total + item.price * item.quantity, 0)
    }, [cart])

    return (
        <div>
            <div className="page-com">
                {menus.map(menu => (
                    <MenuCard key={menu.name} menu={menu} addToCart={addToCart} />
                ))}
            </div>
            <div>
                {cart.map(item => (
                    <CartItem key={item.name} item={item} removeFromCart={removeFromCart} />
                ))}
            </div>
            <div>총 금액 : {calculateTotal()}원</div>
        </div>
    )
}

export default TestPage1
