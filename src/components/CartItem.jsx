import React from 'react';

const CartItem = ({ item, removeFromCart }) => {
    return (
        <div key={item.name}>
            {item.name} : {item.quantity}개<br/>
            {item.quantity * item.price}원
            <button onClick={() => removeFromCart(item)}>삭제</button>
        </div>
    )
}

export default CartItem;