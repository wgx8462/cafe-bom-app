import React from 'react';

const MenuCard = ({ menu, addToCart }) => (
    <div onClick={() => {addToCart(menu)}} className="page-com-item">
        <img src={process.env.PUBLIC_URL + '/images/' + menu.imgUrl} alt="커피이미지" width={150} height={150}/>
        <div>{menu.name}</div>
        <div>{menu.price}</div>
    </div>
)

export default MenuCard;